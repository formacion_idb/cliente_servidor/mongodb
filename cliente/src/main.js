const {BrowserWindow, ipcMain} = require('electron')

exports.createWindow = () => {
    window = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        },
        height: 600,
        width: 800
    })
    //window.maximize()
    window.show()
    window.loadFile('src/ui/index.html')
}