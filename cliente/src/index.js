const {createWindow} = require('./main')
const { ipcMain, dialog } = require('electron')
const http = require('http');

const {app} = require('electron')
app.disableHardwareAcceleration();

app.whenReady().then(createWindow)


app.whenReady().then(() => { 
    ipcMain.handle('getCounter', async (event) => {
      try {
        const response = await fetch('http://127.0.0.1:8080/counter');
        const data = await response.json();
        return data;
      } catch (error) {
        throw error;
      }
    });
    ipcMain.handle('getMaxScore',async (event) => {
        try{
            const response = await fetch('http://localhost:8080/maxCounter');
            const data = await response.json();
            return data;
        }catch (error) {
            throw error;
        }
    });
    ipcMain.handle('open-file-dialog', async (event) => {
    const result = await dialog.showOpenDialog({
      properties: ['openFile']
    });

    console.log(result); 
    
    if (!result.canceled && result.filePaths.length > 0) {
      return result.filePaths[0];
      
    }
    
    throw new Error('No file selected');
  });

ipcMain.handle('getVisitors', async (event) =>{
  try {
    const response = await fetch('http://127.0.0.1:8080/visitors');
    const data = await response.json();
    return data;
  } catch (error) {
    throw error;
  }
});     
});
