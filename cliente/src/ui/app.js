const { ipcRenderer } = require('electron');
const fs = require('fs');
const http = require('http');


const visitorNameInput = document.getElementById('visitorNameInput');
const addVisitorButton = document.getElementById('addVisitorButton');
const getDocumentButton = document.getElementById('getDocumentButton');
const documentDataElement = document.getElementById('documentData'); 
const fileUpload = document.getElementById('fileUpload');
const uploadButton = document.getElementById('uploadButton'); 

ipcRenderer.invoke('getCounter')
  .then((data) => {
    document.getElementById('score').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });

ipcRenderer.invoke('getMaxScore')
  .then((data) => {
    document.getElementById('maxScore').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });

const refreshCounters = () => {
ipcRenderer.invoke('getCounter')
    .then((data) => {
    document.getElementById('score').textContent = data;
    })
    .catch((err) => {
    console.log(err);
    });

    ipcRenderer.invoke('getMaxScore')
  .then((data) => {
    document.getElementById('maxScore').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });
};



document.addEventListener('DOMContentLoaded', () => {
const add1Button = document.getElementById('add1Button');
add1Button.addEventListener('click', () => {
  add1ToCounter();
});
const add1ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/add1/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const add10Button = document.getElementById('add10Button');
add10Button.addEventListener('click', () => {
  add10ToCounter();
});
const add10ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/add10/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const subtract1Button = document.getElementById('Subtract1Button');
subtract1Button.addEventListener('click', () => {
  subtract1ToCounter();
});
const subtract1ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/subtract1/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const subtract10Button = document.getElementById('Subtract10Button');
subtract10Button.addEventListener('click', () => {
  subtract10ToCounter();
});
const subtract10ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/subtract10/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const restartCounter = document.getElementById('ResetButton');
restartCounter.addEventListener('click', () => {
  resetCounter();
});
const resetCounter = async () => {
  try {
    await fetch('http://localhost:8080/counter', {
      method: 'DELETE'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};

});






setInterval(refreshCounters, 1000);
